<!DOCTYPE html>
<html>
<head>

</head>

<body>
    <script>
    <?php
        // since we know that the agent will require to be provisioned, we
        // we can eagerly provision a session token on the page load rather
        // than do so lazily later

        // include the Provisioner class we created to do the provisioning
        require_once('Provisioner.php');
        $provisioner = new Provisioner;
        $token = $provisioner->provisionAgent(
            'agent1',
            'example.com',
            '.*');


        // decode the sessionID from the provisioning token
        $sessionId = json_decode($token)->sessionid;
        ?>

        var sessionId = '<?php echo $sessionId; ?>';

        // just for debugging
        alert(sessionId);
    </script>
</body>
</html>
